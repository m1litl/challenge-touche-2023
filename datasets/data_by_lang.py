import pandas as pd


def full_data_by_language(path_data, language):
    """
    from a data, returns a dataframe filtered by language
    :param path_data: path from a file to get the full data
    :param language: str valide to indicate the language
    :return:
    """
    data = pd.read_csv(path_data, sep="\t")
    data_by_language = data[(data["lan"] == language)]
    return data_by_language


def filter_data_by_language(path_data, language):
    """
    :param path_data: path from a file to get the comments
    :param language: text language valide from the languages list in the Stanza models https://stanfordnlp.github.io/stanza/available_models.html#available-ud-models
    :return: two lists; one with list of comments and the other with a list of the ids from these comments
    """
    data_by_language = full_data_by_language(path_data, language)
    comments_by_language = []
    only_comments = []
    comments = list(data_by_language["comment"])
    ids_by_language = list(data_by_language["id"])
    for i in range(0, len(comments)):
        comments_by_language.append(comments[i])
        only_comments.append(comments[i])
        comments_by_language.append("\t\t")
        ids_by_language.append(ids_by_language[i])
    return comments_by_language, ids_by_language, only_comments
