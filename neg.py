import sys
import re


neg_against = 0
neg_in_favor = 0
neg_against_fr=0
neg_against_es=0
neg_against_en=0
neg_in_favor_fr = 0
neg_in_favor_es = 0
neg_in_favor_en = 0
com = 0
many_neg=0


for line in sys.stdin:
	line = line.rstrip("\n")

	l = line.split("\t")

	if l[0].startswith('<comment_'):	# On a un commentaire qui répond directement à la proposition
		com = 1
		match = re.search(r"<comment_(\d{5}) alignment=\'(In favor|Against)\' langue=\'(..)\'>", l[0])
		if match:
			numero = match.group(1)
			opinion = match.group(2)
			langue = match.group(3)
	elif l[0].startswith('</comment_'):
		com = 0
		many_neg = 0

		 
	if opinion == "Against":
		if len(l) == 10 and l[5] == "Polarity=Neg" and many_neg==0:
			many_neg=1
			neg_against=neg_against+1
			if langue == "es":
				neg_against_es=neg_against_es+1
			if langue == "en":
				neg_against_en=neg_against_en+1
			if langue == "fr":
				neg_against_fr=neg_against_fr+1

			
	elif opinion == "In favor":
		if len(l) == 10 and l[5] == "Polarity=Neg" and many_neg==0:
			many_neg = 1
			neg_in_favor=neg_in_favor+1
			if langue == "es":
				neg_in_favor_es=neg_in_favor_es+1
			if langue == "en":
				neg_in_favor_en=neg_in_favor_en+1
			if langue == "fr":
				neg_in_favor_fr=neg_in_favor_fr+1

print(f"Against négation total: {neg_against}")
print(f"Against négation espagnol: {neg_against_es}")
print(f"Against négation anglais: {neg_against_en}")
print(f"Against négation français: {neg_against_fr}")
print(f"In favor négation total: {neg_in_favor}")
print(f"In favor négation espagnol: {neg_in_favor_es}")
print(f"In favor négation anglais: {neg_in_favor_en}")
print(f"In favor négation français: {neg_in_favor_fr}")

