# Description des données en espagnol (es)

## Description générale des données en espagnol

Les commentaires en espagnol sont répartis sur 3 jeu de données (désormais datasets), nous proposons un aperçu de la répartition des ces données dans le tableau ci-dessous :

|Dataset|Nombre de commentaires en espagnol|Pourcentage par rapport au dataset|Pour (et %)|Contre (et %)|Neutre (et %)|
|---|---|---|---|---|---|
|CFs|308|4.40%|276 (89.61%)|32 (10.39%)|X|
|CFu|623|4.72%|X|X|X|
|CFe-d|139|9.83%|88 (63.31%)|6 (4.32%)|45 (32.37%)|

## Description détaillée du dataset CFs

Le dataset CFs contient 308 commentaires rédigés en espagnol, et répartis sur les 10 sujets disponibles de la façon suivante :


|Topic|Nombre de commentaires en espagnol|Pourcentage par rapport au dataset|Pour (et %)|Contre (et %)|
|---|---|---|---|---|
|Migration|14|4.55%|8 (57.14%)|6 (42.86%)|
|GreenDeal|17|5.52%|17 (100.00%)|0 (0.00%)|
|Health|7|2.27%|7 (100.00%)|0 (0.00%)|
|Economy|36|11.69%|35 (97.22%)|1 (2.78%)|
|EUInTheWorld|13|4.22%|10 (76.92%)|3 (23.08%)|
|ValuesRights|50|16.23%|46 (92.00%)|4 (8.00%)|
|Digital|11|3.57%|11 (100.00%)|0 (0.00%)|
|Democracy|91|29.55%|77 (84.62%)|14 (15.38%)|
|Education|43|13.96%|42 (97.67%)|1 (2.33%)|
|OtherIdeas|26|8.44%|23 (88.46%)|3 (11.54%)|

Dans le jeu de données d'apprentissage, sur les 10 catégories, il n'y a que 7 catégories avec l'étiquette "contre". Les thèmes sans exemples sont **"santé"**, **"GreenDeal"** et **"Digital"**.


## Observations linguistiques :

TXM et le fichier "datasets/TXM index alignement.py" ont été utilisés pour les observations suivantes:

-	Spécificités des mots pour les ‘alignment’ ‘in favor’ et ‘against’ sur TXM. A titre d’illustration, le mot ‘no’ a une sur représentation de 5,1 pour la catégorie ‘against’.
-	Spécificités des mots par catégorie grammaticale (sur TXM). La négation a une spécificité de 6,2 pour la catégorie ‘against’. Les adjectifs sont surreprésentés pour la catégorie ‘in favor’ avec une spécificité de 2,9.  
-	Eléments lexicaux que Medina (2012) considère comme relevant de l’opinion, comme par exemple des éléments de minimisation lexicale (‘poco’ = ‘peu’), des adverbes de probabilité (‘quizás’ = ‘peut-être’) ou des verbes dénotant des processus mentaux pour introduire l’opinion (‘pensar’ = ‘penser’).
-	Trigrammes les plus fréquents pour ‘in favor’ et ‘against’ sur TXM. Le trigramme ‘totalmente de acuerdo’ (complétement d’accord) est l’un des plus représentés dans la catégorie ‘in favor’.


## Bibliographie


Calvo, V. D. (2013, 22 avril). RUA : Clasificación de polaridad en textos con opiniones en español mediante análisis sintáctico de dependencias. http://rua.ua.es/. http://rua.ua.es/dspace/handle/10045/27859

Henriquez Miranda, C., & Buelvas, E. (2019). Aspect SA: Unsupervised system for aspect-based sentiment analysis in Spanish. Prospectiva, 17(1), 87 95. https://doi.org/10.15665/rp.v17i1.1961

Martínez Cámara, E., Martín Valdivia, M., Perea Ortega, J. M., & Ureña López, L. A. (2011). Técnicas de clasificación de opiniones aplicadas a un corpus en español Procesamiento del Lenguaje Natural. Sociedad Española Para El Procesamiento Del Lenguaje Natural España, 47, 163 170. https://www.redalyc.org/pdf/5157/515751747017.pdf

Martínez Cámara, E., (2016). Análisis de Opiniones en español. Procesamiento del Lenguaje Natural, (56), 103-106.

Medina, I. (2012). Los elementos atenuadores para expresar desacuerdo en el discurso oral de estudiantes E/LE universitarios de nivel B1 en contexto de inmersión. Revista Nebrija de Lingüística Aplicada a la Enseñanza de Lenguas, (11), 97-138.

Medhat, W., Hassan, A., & Korashy, H. (2014). Sentiment analysis algorithms and applications : A survey. Ain Shams Engineering Journal, 5(4), 1093‑1113. https://doi.org/10.1016/j.asej.2014.04.011

Moreno Sandoval, A. (2014). Análisis de opinión y contenido en los medios sociales. 10.13140/2.1.3585.7928.

Zafra, S. J. M. (2015, 4 mars). RUA : Tratamiento de la Negación en el Análisis de Opiniones en Español. http://rua.ua.es/dspace/handle/10045/45492

Zotova, E. et al., (2020). Multilingual Stance Detection in Tweets: The Catalonia Independence Corpus. In Proceedings of the Twelfth Language Resources and Evaluation Conference, pp. 1368–1375, Marseille, France. European Language Resources Association.