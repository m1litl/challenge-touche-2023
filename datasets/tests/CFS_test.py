import unittest
import re
from datasets.data_by_lang import *

CFS = "CFS.tsv"
CFU = "CFU.tsv"
analysed_CFS = "stanza_analyzed_data/CFS_analysed_sample.txt"
analysed_CFU = "stanza_analyzed_data/CFU_analysed_sample.txt"


def analysed_comments_validation(full_comments, lang):
    analysed_comments_by_lang = []
    file = open(full_comments)
    for line in file:
        line = line.rstrip("\n")
        split_line = line.split("\t")
        if len(split_line) != 10 and line != '':
            comment_head = re.search(rf"<(comment_.+) alignment=.+ langue='({lang})'>", line)
            if comment_head:
                analysed_comments_by_lang.append(comment_head.group(2))
        else:
            pass
    file.close()
    return analysed_comments_by_lang


def check_current(folder_path):
    current_comments = set()
    file = open(folder_path)
    for ligne in file:
        ligne = ligne.rstrip("\n")
        if ligne.startswith("<comment_"):
            comment_match = re.search(r"<(comment_.+) alignment.+>", ligne)
            if comment_match:
                comment_name = comment_match.group(1)
                current_comments.add(comment_name)
    file.close()
    return current_comments


def full_expected_comments(data):
    expected_comments = set(data["id"])
    return expected_comments


def lang_identification_difference(difference, data):
    freq = dict()
    for comment in difference:
        row = data.loc[data['id'] == comment]
        lang = row["lan"].to_string(index=False)
        freq[lang] = freq.get(lang, 0) + 1
    return freq

def data_not_analysed(folder_path, data_metadata):
    data = pd.read_csv(data_metadata, sep="\t")
    current_comments = check_current(folder_path)
    expected_comments = full_expected_comments(data)
    difference = expected_comments - current_comments
    full_info_from_difference = lang_identification_difference(difference, data)
    return full_info_from_difference

class StanzaAnalyseCFSTest(unittest.TestCase):
    def test_CFS_full_analyse(self):
        no_analysed_data = data_not_analysed(analysed_CFU, CFU)
        self.assertEqual(no_analysed_data, {'eo': 20})

    def test_CFU_complete_analyse(self):
        no_analysed_data = data_not_analysed(analysed_CFS, CFS)
        self.assertEqual(no_analysed_data, {'eo': 20})
    def test_en_CFS_comments_analysed(self):
        en_comments = filter_data_by_language(CFS, "en")
        en_analysed = analysed_comments_validation(analysed_CFS, "en")
        self.assertEqual(len(en_analysed), len(en_comments[2]))

    def test_fr_CFS_comments_analysed(self):
        fr_comments = filter_data_by_language(CFS, "fr")
        fr_analysed = analysed_comments_validation(analysed_CFS, "fr")
        self.assertEqual(len(fr_analysed), len(fr_comments[2]))

    def test_es_CFS_comments_analysed(self):
        es_comments = filter_data_by_language(CFS, "es")
        es_analysed = analysed_comments_validation(analysed_CFS, "es")
        self.assertEqual(len(es_analysed), len(es_comments[2]))

    def test_de_CFS_comments_analysed(self):
        de_comments = filter_data_by_language(CFS, "de")
        de_analysed = analysed_comments_validation(analysed_CFS, "de")
        self.assertEqual(len(de_analysed), len(de_comments[2]))

    def test_it_CFS_comments_analysed(self):
        it_comments = filter_data_by_language(CFS, "it")
        it_analysed = analysed_comments_validation(analysed_CFS, "it")
        self.assertEqual(len(it_analysed), len(it_comments[2]))

    def test_nl_CFS_comments_analysed(self):
        nl_comments = filter_data_by_language(CFS, "nl")
        nl_analysed = analysed_comments_validation(analysed_CFS, "nl")
        self.assertEqual(len(nl_analysed), len(nl_comments[2]))


    def test_ro_CFS_comments_analysed(self):
        ro_comments = filter_data_by_language(CFS, "ro")
        ro_analysed = analysed_comments_validation(analysed_CFS, "ro")
        self.assertEqual(len(ro_analysed), len(ro_comments[2]))

    def test_hr_CFS_comments_analysed(self):
        hr_comments = filter_data_by_language(CFS, "hr")
        hr_analysed = analysed_comments_validation(analysed_CFS, "hr")
        self.assertEqual(len(hr_analysed), len(hr_comments[2]))

    def test_pl_CFS_comments_analysed(self):
        pl_comments = filter_data_by_language(CFS, "pl")
        pl_analysed = analysed_comments_validation(analysed_CFS, "pl")
        self.assertEqual(len(pl_analysed), len(pl_comments[0]))

    def test_hu_CFS_comments_analysed(self):
        hu_comments = filter_data_by_language(CFS, "hu")
        hu_analysed = analysed_comments_validation(analysed_CFS, "hu")
        self.assertEqual(len(hu_analysed), len(hu_comments[0]))





if __name__ == '__main__':
    unittest.main()
