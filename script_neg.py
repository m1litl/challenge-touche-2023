import sys
import re

Id=[""]
id_prop=[""]
alignment=[""]
tokens=[""]
sentences=[""]
topic=[""]
language=[""]
data_set=[""]
comment=[""]
freq=dict()
n=0
o=0
p=0

for ligne in sys.stdin:
	ligne = ligne.rstrip("\n")
	ligne_liste = ligne.split("\t")
	if len(ligne_liste) == 9:
		if "-" not in ligne_liste[0]:

			Id.append(ligne_liste[0])
			id_prop.append(ligne_liste[1])
			alignment.append(ligne_liste[2])
			tokens.append(ligne_liste[3])
			sentences.append(ligne_liste[4])
			topic.append(ligne_liste[5])
			language.append(ligne_liste[6])
			data_set.append(ligne_liste[7])
			comment.append(ligne_liste[8])
	
for i in range (0,len(comment)):
	content=comment[i]
	if alignment[i] in ("Against"):
		if  language[i] in ("en") :
			if re.search (r"(\snot\s|\sno\s|.+(n't))",content) != None:
				n+=1
			else:
				contentListEN = content.split(" ")
				negativeWordsEN = ["none","neither","nor", "never","no one","nobody","nothing","nowhere","declined", "denied","cannot","unlikely"]

				for word in negativeWordsEN:
					if word in contentListEN:
						n+=1

		elif language[i] in ("fr"):
			if re.search (r"((n'|ne|non)\s.*\s(pas|plus|rien))",content) != None:
				o+=1
			else:
				contentListFR = content.split(" ")
				negativeWordsFR = ["rien","jamais","personne", "pas encore","nulle part","aucun","guère","nullement"]
				
				for word in negativeWordsFR:
					if word in contentListFR:
						o+=1

		elif language[i] in ("es"):
			if re.search (r"(no\s.*\sni)",content) != None:
				p+=1
			else:
				contentListES = content.split(" ")
				negativeWordsES = ["nadie","nada","nunca", "jamás","ningún","no"]
				
				for word in negativeWordsES:
					if word in contentListES:
						p+=1

print("Nb commentaires Anglais:", n)
print("Nb commentaires Français:", o)
print("Nb commentaires Espagnol:", p)
					
#Nb commentaires Against longs  Anglais: 13/25
#Nb commentaires Against longs Français: 20/25
#Nb commentaires Against longs Espagnol: 15/25

#Nb commentaires Against courts Anglais: 2/21
#Nb commentaires Against courts Français: 0/2
#Nb commentaires Against courts Espagnol: 1/1

#Nb commentaires in Favor longs  Anglais: 8/25
#Nb commentaires in Favor longs Français: 10/25
#Nb commentaires in Favor longs Espagnol: 12/25

#Nb commentaires in Favor courts Anglais: 0/25
#Nb commentaires in Favor courts Français: 0/26
#Nb commentaires in Favor courts Espagnol: 0/25


