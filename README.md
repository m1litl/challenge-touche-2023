# Intra-Multilingual Multi-Target Stance Classification 2023

![Beatrix Kiddo M1 LITL@Touché 2023](https://drive.google.com/uc?export=view&id=1VJcRv3VH7edmcn3USXUlMXt-fVQ_p2aD)

* [Articles](#articles)
* [Datasets](#datasets)
    * [Dataset CFS](#dataset-cfs)
* [Programmes](#programmes)

## Articles

Le repo contient 3 articles et un compte rendu sur la racine, ainsi qu'un dossier articles permettant de stocker les références futures dans l'avancement de notre travail sur le projet.

Les 3 articles actuellement présents sont des articles référencés dans la présentation des données de CoFE que nous allons utiliser pour la [tâche 4](https://touche.webis.de/clef23/touche23-web/multilingual-stance-classification.html "Intra-Multilingual Multi-Target Stance Classification 2023") du challenge [Touché](https://touche.webis.de/clef23/touche23-web/index.html "Touché et CLEF 2023"). Chaque article a été lu par un sous-groupe et nous a permis d'avoir un rapide état de l'art et un aperçu des données similaires aux données du CoFE.

Les références bibliographiques de ces 3 articles sont :
* M. Hosseinia, E. Dragut, and A. Mukherjee. *Stance Prediction for Contemporary Issues: Data and Experiments*. 2020. (Article 2)
* J.-C. Klie, M. Bugert, B. Boullosa, R. E. de Castilho, and I. Gurevych. The INCEpTION Platform: Machine-Assisted and Knowledge-Oriented Interactive Annotation. In *Proceedings of the International Conference on Computational Linguistics*, pages 5–9, 2018. (Article 3)
* J. Vamvas and R. Sennrich. X-stance: A Multilingual Multi-Target Dataset for Stance Detection. In *SwissText*, 2020.

Le dernier fichier pdf (Debating_Europe._Anca__Charlotte__Dani.docx.pdf) consiste en un compte-rendu des lectures d'un autre article référencé dans la présentation des données :
* V. Barriere, A. Balahur, and B. Ravenet. Debating Europe : A Multilingual Multi-Target Stance Classification Dataset of Online Debates. In *Proceedings of the First Workshop on Natural Language Processing for Political Sciences (PoliticalNLP)*, LREC, number June, pages 16–21, Marseille, France, 2022. European Language Resources Association.

## Datasets

Le dossier *datasets* contient les fichiers CFS.tsv, CFU.tsv, et CFE-D.tsv, correspondant aux 3 sets de données présents sur le [site de la tâche](https://touche.webis.de/clef23/touche23-web/multilingual-stance-classification.html#data "Données pour la tâche 4").

Pour rappel, ces données peuvent être décrites de la façon suivante :
* **CFs**: 7,000 commentaires classés en pour ou contre par leur propre auteur.
* **CFu**: commentaires sans annotations
* **CFe-d**: 1,200 commentaires classés par INCEpTION en pour, contre ou neutre. Nous y trouvons des commentaires seulement en français, allemand, anglais, et espagnol.

### Dataset CFS

Le reste des fichiers du dossier *datasets* correspondent à une découpe du fichier complet CFS.tsv en langue, afin d'avoir un aperçu des différentes langues présentes de façon ordonnée. Ces fichiers ont été générés à l'aide du programme Python *basic_slicer.py* présent à la racine. (voir [Programmes](#programmes))

Afin de pousser ensuite l'observation des données, nous avons sélectionné 3 langues (anglais, français, espagnol) à analyser, et nous avons déposé les fichiers correspondant dans un dossier par langue, chacun de ces dossiers se trouvant dans *datasets/CFS* (*CFS_en* pour l'anglais, *CFS_fr* pour le français, *CFS_es* pour l'espagnol).

Dans chacun de ces dossiers se trouvent 4 fichiers :
* Le fichier complet contenant tous les commentaires de la langue étudiée, au format tsv.
* Un fichier contenant les commentaires "in favor" pour la langue étudiée, au format tsv.
* Un fichier contenant les commentaires "against" pour la langue étudiée, au format tsv.
* Un fichier contenant tous les commentaires de la langue étudiée formatés suivant la norme Alceste afin de pouvoir l'importer dans TXM pour effectuer les analyses, au format txt. Les commentaires sont accompagnés de métadonnées renseignant sur l'identifiant du commentaire (comid), le positionnement du commentaire (alignment), ainsi que le sujet sur lequel porte le commentaire (topic).

Ces fichiers ont été générés à l'aide du programme Python *txm_ready_alceste_slicer.py*. (voir [Programmes](#programmes))

En plus de ces fichiers, nous y trouvons également une description générale des données de la langue correspondante dans un fichier .md. Les données quantitatives présentes dans ces descriptions ont été générées à l'aide du programme *inspection_quanti.py*. (voir [Programmes](#programmes))

## Programmes

Le repo contient 3 programmes à la racine qui ont permis de générer plusieurs fichiers et informations essentielles à notre avancée dans le projet. Les 3 programmes sont décrits ci-dessous.
* *basic_slicer.py* : Ce slicer basique génère un fichier tsv par langue, chacun contenant uniquement les commentaires associés à cette langue, en prenant le fichier complet CFS.tsv en entrée.
* *txm_ready_alceste_slicer.py* : Ce slicer plus avancé génère 4 fichiers (voir [ci-dessus](#dataset-cfs)) dont un fichier prêt à être importé sur TXM pour analyse.
* *inspection_quanti.py* : Ce programme créé deux tableaux (un tableau général prenant en compte les 3 datasets, et un tableau spécifique aux données du dataset **CFs**) au format markdown qui sont directement utilisable dans les fichiers de description pour chaque langue.


![UT2J](https://authc.univ-toulouse.fr/assets/logos/ut2-a933aeba56cf90831cd94e724d1d3982.png)