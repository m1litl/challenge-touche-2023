import sys
import re
import string
"""

def lemmatization():
    # Créer une chaîne de caractères vide
	commentaire = ""
	for ligne in sys.stdin:
		ligne = ligne.rstrip("\n")
		t = ligne.split("\t")
		if len(t) > 1:
			if t[2] in string.punctuation:
            # Ajouter la ponctuation à la chaîne de caractères
				commentaire += t[2]
			else:
            # Ajouter le token à la chaîne de caractères en ajoutant un espace avant
				commentaire += " " + t[2]

		elif t[0] == "":
			print(commentaire)
			commentaire = ""
"""

#Fonction qui calcule le score d'accord/désaccord d'un commentaire en utilisant des listes de regex d'expressions explicites pour et contre
def score_commentaire(commentaire, liste_pour, liste_contre):
	score = 0.0

	for regex in liste_pour:
		match = re.search(regex, commentaire)
		if match:
			print(match)
			score += 1/5
	for regex in liste_contre:
		match = re.search(regex, commentaire)
		if match:
			print(match)
			score -= 1/5

	return score



#Liste regex français pour et contre:
liste_pour_fr = [r"(j|J)e\ssuis\s(.+)?favorable", r"(j|J)e\ssuis\s(.+)?d'accord", r"(j|J)e\ssuis\s(.+)?pour"]
liste_contre_fr = [r"(j|J)e\ssuis\s(.+)?opposée?", r"(j|J)e\ssuis\s(.+)?contre", r"(j|J)e(\sne)?\ssuis\spas\s(.+)?favorable", r"(j|J)e(\sne)?\ssuis\spas\s(.+)?d'accord", r"(j|J)e(\sne)?\ssuis\spas\s(.+)?pour"]

#liste regex catalan pour et contre:
liste_pour_catalan = [r"(j|J)o\s(estic|suporto)\s(d’acord)?", r"estic\sa\sfavor\sde"] 
liste_contre_catalan = [r"(j|J)o\s(no\sestic\sd'acord\s(amb)?|rebutjo)\s", r"estic\sen\scontra\sde"]

#liste regex danois pour et contre:
liste_pour_dk = [r"(j|J)eg\s(er|går)\s(ind\s)?(enig|for)\s", r"aftalt", r"helt\senig\s"]
liste_contre_dk = [r"(j|J)eg\s(er|går)\s(imod|ikke)\s(enig|ind)?(\sfor)?", r"\b(j|J)eg\s afviser\s"]

#liste regex hongrois pour et contre:
liste_pour_hu = [r"(en)?\stámogatom\s", r"(teljesen)?\segyetértek\s(vele)?\s"]
liste_contre_hu = [r"(en)?\sellenzem\sa?\s", r"nem\sertek(\svele)?(\sezzel)?\segyet\s", r"(elutasitom|visszautasítom)"]

#liste regex Tchèque pour et contre:
liste_pour_cz = [r"(já)?\sjesem\spro\s", r"(naprosto|naprostý|zcela)?\bsouhlas(ím)?\s", r"souhlas(í|il)|dohodnuto"]
liste_contre_cz = [r"jsem\sproti(\stomu)?\s", r"nesouhlasim"]

#liste regex Slovaque pour et contre:
liste_pour_sk = [r"(s|S)úhlasím\s", r"(ja\s)?som\sza\s", r"celkom|úplne|dost"]
liste_contre_sk = [r"(n|N)esúhlasím\s(s\s)?(tým\s)?", r"(s|S)om\sproti\s(tomu\s)?"]

#liste regex Slovène pour et contre:
liste_pour_si = [r"(s|S)trinjam\sse\s", r"(P|p)odpiram", r"(S|s)em\sza\s", r"dogovorjeno"]
liste_contre_si = [r"(N|n)asprotujem", r"(s|S)em\sproti\s", r"(se\s)?(N|n)e\sstrinjam\s(se\s)?"]

#liste regex irlandais, anglais pour et contre:
liste_pour_en = []
liste_contre_en = []

#liste regex polonais pour et contre:
liste_pour_pl = [r"[zZ]gadzam\ssi[ęe]\sz\stym", r"[dD]obry\spomys[łl]", r"[wW]spaniał[ae]\s.*inicjatyw[ae]", r"[Rr]ozsądne\srozwiązanie", r"^(?!.*nie)[jJ]estem\szadowolon[ya]"]
liste_contre_pl = [r"([tT]o)?\snie\sjest\sdobry\spomys[łl]", r"([Tt]en)?\spomys[łl]\sjest\sz[łl]y", r"[tT]o.*\snie\sjest\sdobra\sinicjatywa", r"[tT]o\srozwi[ąa]zanie\snie\sjest\srozs[ąa]dne", r"[nN]ie\sjestem\szadowolon[ya]"]

#liste regex néerlandais pour et contre:
liste_pour_nl = [r"(helemaal\s)?(?<!niet\s)((mee\s)?(?<!niet mee\s)eens)", r"(?<!niet\s)(heel\s)?(?<!niet heel\s)(?<!niet een\s)(?<!niet een heel\s)goed idee"]
liste_contre_nl = [r"niet\s(\w+\s)?akkoord", r"(?<!niet\s)tegen", r"(?<!niet\s)(heel\s)?(?<!niet heel\s)(?<!niet een\s)(?<!niet een heel\s)slecht idee", r"(?<!niet\s)akkoord"]

#liste regex italien pour et contre:
liste_pour_it = []
liste_contre_it = []

#liste regex bulgare pour et contre:
liste_pour_bg = []
liste_contre_bg = []

#liste regex esperanto pour et contre:
liste_pour_esperanto = []
liste_contre_esperanto = []

#liste regex portugais pour et contre:
liste_pour_pt = []
liste_contre_pt = []

#liste regex estonien pour et contre:
liste_pour_ee = []
liste_contre_ee = []

#liste regex finnois pour et contre:
liste_pour_fi = []
liste_contre_fi = []

#liste regex suédois pour et contre:
liste_pour_se = []
liste_contre_se = []

#liste regex croate pour et contre:
liste_pour_hr = [r"slažem se", r"saglas(an|na)\s+sam", r"u potpunosti se slažem", r"podržavam", r"puna podrška", r"vašeg sam mišljenja", r"dijelim(\s+vaše)?\s+mišljenje", r"ideja\s+je\s+(sjajna|izvrsna|dobra|odlična)"]
liste_contre_hr = [r"ne slažem\s+se", r"protivim\s+se", r"ne podržavam", r"nisam\s+saglas(an|na)", r"u potpunosti\s+sam\s+protiv", r"nisam\s+vašeg\s+mišljenja", r"ne dijelim(\s+vaše)?\s+mišljenje", r"ideja\s+je\s+(loša|grozna|užasna|glupa)"]

#liste regex allemand pour et contre:
liste_pour_de = []
liste_contre_de = []

#liste regex roumain pour et contre:
liste_pour_ro = []
liste_contre_ro = []

#liste regex espagnol pour et contre:
liste_pour_es = []
liste_contre_es = []

#liste regex letton pour et contre:
liste_pour_lv = []
liste_contre_lv = []

#liste regex lituanien pour et contre:
liste_pour_lt = []
liste_contre_lt = []

#liste regex grec pour et contre:
liste_pour_gr = []
liste_contre_gr = []


com = 0		#indicateur de si on est en train de lire les phrases du commentaire ou non
commentaire = ""
score_expression_explicite = 0.0
#code qui extrait seulement les textes bruts des commentaires
for ligne in sys.stdin:
	ligne = ligne.rstrip("\n")
	t = ligne.split("\t")

	if t[0].startswith('<comment_'):	# On a un commentaire en texte brut
		com = 1
		match = re.search(r"<comment_(\d+) alignment=\'(In favor|Against)\' langue=\'(..)\'>", t[0])
		if match:
			numero = match.group(1)
			langue =  match.group(3)

	elif com == 1 and t[0].startswith('# text = '):		# On prend toutes les phrases du commentaire
		commentaire = commentaire + t[0][9:] + " "

	elif "</comment_" in t[0]:		#Quand on a fini de lire toutes les phrases du commentaire, on traite puis on réinitialise

		if langue == "fr":
			print(commentaire)
			score_expression_explicite = score_commentaire(commentaire, liste_pour_fr, liste_contre_fr)
			print("Commentaire n°", numero, "langue=", langue, "score expressions explicites =", score_expression_explicite)
			print("\n")

		com = 0
		commentaire = ""
		numero = 0
		langue = ""




#lemmatization()
