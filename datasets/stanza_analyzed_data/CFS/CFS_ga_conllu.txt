<comment_215920 alignment='In favor' langue='ga'>
# text = Tha a moladh seo glè chudromach.
# sent_id = 0
1	Tha	tha	PUNCT	Punct	_	3	punct	_	start_char=0|end_char=3
2	a	a	DET	Art	Definite=Def|Number=Sing|PronType=Art	3	det	_	start_char=4|end_char=5
3	moladh	moladh	NOUN	Noun	Case=NomAcc|Definite=Def|Gender=Masc|Number=Sing|VerbForm=Inf	0	root	_	start_char=6|end_char=12
4	seo	seo	DET	Det	PronType=Dem	3	det	_	start_char=13|end_char=16
5	glè	glè	NOUN	Noun	Case=NomAcc|Gender=Fem|Number=Sing	3	nsubj	_	start_char=17|end_char=20
6	chudromach	cudromach	ADJ	Adj	Case=NomAcc|Form=Len|Gender=Fem|Number=Sing	5	amod	_	start_char=21|end_char=31
7	.	.	PUNCT	.	_	1	punct	_	start_char=31|end_char=32

# text = Bu chòir taic a thoirt do chànanan na Roinn Eòrpa agus tha dleastanas air an EU taic a thoirt do mhion-chànanan choimhearsnachdan na Roinn Eòrpa
# sent_id = 1
1	Bu	is	AUX	Cop	Polarity=Neg|Tense=Pres|VerbForm=Cop	2	cop	_	start_char=33|end_char=35
2	chòir	chòir	ADJ	Adj	Degree=Pos|Form=Len	0	root	_	start_char=36|end_char=41
3	taic	taic	NOUN	Noun	Case=NomAcc|Definite=Def|Gender=Masc|Number=Sing	2	nsubj	_	start_char=42|end_char=46
4	a	a	DET	Det	Gender=Masc|Number=Sing|Person=3|Poss=Yes	5	nmod:poss	_	start_char=47|end_char=48
5	thoirt	toirt	NOUN	Noun	Form=Len|VerbForm=Inf	3	nmod	_	start_char=49|end_char=55
6	do	do	ADP	Simp	_	7	case	_	start_char=56|end_char=58
7	chànanan	cànanan	NOUN	Noun	Case=NomAcc|Definite=Def|Form=Len|Gender=Masc|Number=Sing	5	obl	_	start_char=59|end_char=67
8	na	an	DET	Art	Case=Gen|Definite=Def|Gender=Fem|Number=Sing|PronType=Art	9	det	_	start_char=68|end_char=70
9	Roinn	roinn	NOUN	Noun	Case=Gen|Definite=Def|Gender=Fem|Number=Sing	7	nmod	_	start_char=71|end_char=76
10	Eòrpa	Eòrpa	PROPN	Noun	Case=Gen|Definite=Def|Gender=Masc|Number=Sing	9	flat	_	start_char=77|end_char=82
11	agus	agus	CCONJ	Coord	_	12	cc	_	start_char=83|end_char=87
12	tha	ta	NOUN	Noun	Case=NomAcc|Form=Len|Gender=Masc|Number=Sing	3	conj	_	start_char=88|end_char=91
13	dleastanas	dleastanas	NOUN	Noun	Case=Gen|Gender=Masc|Number=Sing	12	nmod	_	start_char=92|end_char=102
14	air	ar	ADP	Prep	Gender=Masc|Number=Sing|Person=3	12	obl:prep	_	start_char=103|end_char=106
15	an	an	DET	Art	Definite=Def|Number=Sing|PronType=Art	16	det	_	start_char=107|end_char=109
16	EU	EU	NOUN	Abr	Abbr=Yes	12	nmod	_	start_char=110|end_char=112
17	taic	taic	NOUN	Noun	Case=NomAcc|Gender=Masc|Number=Sing	19	obj	_	start_char=113|end_char=117
18	a	a	PART	Inf	PartType=Inf	19	mark	_	start_char=118|end_char=119
19	thoirt	toirt	NOUN	Noun	Form=Len|VerbForm=Inf	12	xcomp	_	start_char=120|end_char=126
20	do	do	ADP	Simp	_	21	case	_	start_char=127|end_char=129
21	mhion-chànanan	mion-chàànan	NOUN	Noun	Case=NomAcc|Definite=Def|Form=Len|Gender=Masc|Number=Sing	19	obl	_	start_char=130|end_char=144
22	choimhearsnachdan	coimhearsnachd	NOUN	Noun	Case=NomAcc|Form=Len|Gender=Masc|Number=Sing	21	nmod	_	start_char=145|end_char=162
23	na	an	DET	Art	Case=Gen|Definite=Def|Gender=Fem|Number=Sing|PronType=Art	24	det	_	start_char=163|end_char=165
24	Roinn	roinn	NOUN	Noun	Case=Gen|Definite=Def|Gender=Fem|Number=Sing	22	nmod	_	start_char=166|end_char=171
25	Eòrpa	Eòrpa	PROPN	Noun	Case=Gen|Definite=Def|Gender=Masc|Number=Sing	24	flat	_	start_char=172|end_char=177


</comment_215920>
<comment_140857 alignment='In favor' langue='ga'>
# text = Thanks.
# sent_id = 0
1	Thanks	Thanks	X	Foreign	Foreign=Yes	0	root	_	start_char=0|end_char=6
2	.	.	PUNCT	.	_	1	punct	_	start_char=6|end_char=7

# text = Malus Horniacek.
# sent_id = 1
1	Malus	Malus	PROPN	Noun	Foreign=Yes	0	root	_	start_char=8|end_char=13
2	Horniacek	Horniacek	PROPN	Foreign	Foreign=Yes	1	flat:name	_	start_char=14|end_char=23
3	.	.	PUNCT	.	_	1	punct	_	start_char=23|end_char=24

# text = MfG Sascha Becker
# sent_id = 2
1	MfG	MfG	PROPN	Noun	Definite=Def	0	root	_	start_char=25|end_char=28
2	Sascha	Sascha	PROPN	Noun	Definite=Def|Gender=Masc|Number=Sing	1	flat:name	_	start_char=29|end_char=35
3	Becker	Becker	PROPN	Noun	Definite=Def|Gender=Masc|Number=Sing	1	flat:name	_	start_char=36|end_char=42


</comment_140857>
