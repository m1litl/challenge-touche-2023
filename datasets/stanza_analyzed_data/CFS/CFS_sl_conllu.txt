<comment_114445 alignment='In favor' langue='sl'>
# text = Delimo vaše mnenje!
# sent_id = 0
1	Delimo	deliti	VERB	Vmpr1p	Aspect=Imp|Mood=Ind|Number=Plur|Person=1|Tense=Pres|VerbForm=Fin	0	root	_	start_char=0|end_char=6
2	vaše	vaš	DET	Ps2nsap	Case=Acc|Gender=Neut|Number=Sing|Number[psor]=Plur|Person=2|Poss=Yes|PronType=Prs	3	det	_	start_char=7|end_char=11
3	mnenje	mnenje	NOUN	Ncnsa	Case=Acc|Gender=Neut|Number=Sing	1	obj	_	start_char=12|end_char=18
4	!	!	PUNCT	Z	_	1	punct	_	start_char=18|end_char=19

# text = In očitno je, da v tem prepričanju niste osamljeni:
# sent_id = 1
1	In	in	CCONJ	Cc	_	2	cc	_	start_char=20|end_char=22
2	očitno	očiten	ADJ	Agpnsn	Case=Nom|Degree=Pos|Gender=Neut|Number=Sing	0	root	_	start_char=23|end_char=29
3	je	biti	AUX	Va-r3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Pres|VerbForm=Fin	2	cop	_	start_char=30|end_char=32
4	,	,	PUNCT	Z	_	10	punct	_	start_char=32|end_char=33
5	da	da	SCONJ	Cs	_	10	mark	_	start_char=34|end_char=36
6	v	v	ADP	Sl	Case=Loc	8	case	_	start_char=37|end_char=38
7	tem	ta	DET	Pd-nsl	Case=Loc|Gender=Neut|Number=Sing|PronType=Dem	8	det	_	start_char=39|end_char=42
8	prepričanju	prepričanje	NOUN	Ncnsl	Case=Loc|Gender=Neut|Number=Sing	10	obl	_	start_char=43|end_char=54
9	niste	biti	AUX	Va-r2p-y	Mood=Ind|Number=Plur|Person=2|Polarity=Neg|Tense=Pres|VerbForm=Fin	10	cop	_	start_char=55|end_char=60
10	osamljeni	osamljen	ADJ	Appmpn	Case=Nom|Degree=Pos|Gender=Masc|Number=Plur|VerbForm=Part	2	csubj	_	start_char=61|end_char=70
11	:	:	PUNCT	Z	_	2	punct	_	start_char=70|end_char=71

# text = Profesionalizacija in kulturna vzgoja na lokalni ravni ter nacionalne in regionalne finančne spodbude za več mednarodnega kulturnega sodelovanja.
# sent_id = 2
1	Profesionalizacija	profesionalizacija	NOUN	Ncfsn	Case=Nom|Gender=Fem|Number=Sing	0	root	_	start_char=72|end_char=90
2	in	in	CCONJ	Cc	_	4	cc	_	start_char=91|end_char=93
3	kulturna	kulturen	ADJ	Agpfsn	Case=Nom|Degree=Pos|Gender=Fem|Number=Sing	4	amod	_	start_char=94|end_char=102
4	vzgoja	vzgoja	NOUN	Ncfsn	Case=Nom|Gender=Fem|Number=Sing	1	conj	_	start_char=103|end_char=109
5	na	na	ADP	Sl	Case=Loc	7	case	_	start_char=110|end_char=112
6	lokalni	lokalen	ADJ	Agpfsl	Case=Loc|Degree=Pos|Gender=Fem|Number=Sing	7	amod	_	start_char=113|end_char=120
7	ravni	raven	NOUN	Ncfsl	Case=Loc|Gender=Fem|Number=Sing	4	nmod	_	start_char=121|end_char=126
8	ter	ter	CCONJ	Cc	_	13	cc	_	start_char=127|end_char=130
9	nacionalne	nacionalen	ADJ	Agpfpa	Case=Nom|Degree=Pos|Gender=Fem|Number=Plur	13	amod	_	start_char=131|end_char=141
10	in	in	CCONJ	Cc	_	11	cc	_	start_char=142|end_char=144
11	regionalne	regionalen	ADJ	Agpfpn	Case=Nom|Degree=Pos|Gender=Fem|Number=Plur	9	conj	_	start_char=145|end_char=155
12	finančne	finančen	ADJ	Agpfpn	Case=Nom|Degree=Pos|Gender=Fem|Number=Plur	13	amod	_	start_char=156|end_char=164
13	spodbude	spodbuda	NOUN	Ncfpn	Case=Nom|Gender=Fem|Number=Plur	1	conj	_	start_char=165|end_char=173
14	za	za	ADP	Sa	Case=Acc	18	case	_	start_char=174|end_char=176
15	več	več	DET	Rgc	PronType=Ind	18	det	_	start_char=177|end_char=180
16	mednarodnega	mednaroden	ADJ	Agpnsg	Case=Gen|Degree=Pos|Gender=Neut|Number=Sing	18	amod	_	start_char=181|end_char=193
17	kulturnega	kulturen	ADJ	Agpnsg	Case=Gen|Degree=Pos|Gender=Neut|Number=Sing	18	amod	_	start_char=194|end_char=204
18	sodelovanja	sodelovanje	NOUN	Ncnsg	Case=Gen|Gender=Neut|Number=Sing	13	nmod	_	start_char=205|end_char=216
19	.	.	PUNCT	Z	_	13	punct	_	start_char=216|end_char=217

# text = ?locale=sl
# sent_id = 3
1	?locale	?locati	VERB	Vmpp-pf	Aspect=Imp|Gender=Fem|Number=Plur|VerbForm=Part	0	root	_	start_char=217|end_char=224
2	=sl	=sl	PUNCT	Z	_	1	punct	_	start_char=224|end_char=227


</comment_114445>
<comment_241363 alignment='In favor' langue='sl'>
# text = Esperanto je praktičen in pravičen.
# sent_id = 0
1	Esperanto	Esperanta	PROPN	Npmsn	Case=Nom|Gender=Masc|Number=Sing	3	nsubj	_	start_char=0|end_char=9
2	je	biti	AUX	Va-r3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Pres|VerbForm=Fin	3	cop	_	start_char=10|end_char=12
3	praktičen	praktičen	ADJ	Agpmsnn	Case=Nom|Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	0	root	_	start_char=13|end_char=22
4	in	in	CCONJ	Cc	_	5	cc	_	start_char=23|end_char=25
5	pravičen	pravičen	ADJ	Agpmsnn	Case=Nom|Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	3	conj	_	start_char=26|end_char=34
6	.	.	PUNCT	Z	_	3	punct	_	start_char=34|end_char=35

# text = Učenje esperanta kot prvega tujega jezika ščiti materne jezike, hkrati pa propedevtično obogati učno sposobnost za vse jezike.
# sent_id = 1
1	Učenje	učenje	NOUN	Ncnsn	Case=Nom|Gender=Neut|Number=Sing	7	nsubj	_	start_char=36|end_char=42
2	esperanta	esperant	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	1	nmod	_	start_char=43|end_char=52
3	kot	kot	SCONJ	Cs	_	6	case	_	start_char=53|end_char=56
4	prvega	prvi	ADJ	Mlomsg	Case=Gen|Gender=Masc|NumType=Ord|Number=Sing	6	amod	_	start_char=57|end_char=63
5	tujega	tuj	ADJ	Agpmsg	Case=Gen|Degree=Pos|Gender=Masc|Number=Sing	6	amod	_	start_char=64|end_char=70
6	jezika	jezik	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	2	obl	_	start_char=71|end_char=77
7	ščiti	ščititi	VERB	Vmpr3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	start_char=78|end_char=83
8	materne	materen	ADJ	Agpmpa	Case=Acc|Degree=Pos|Gender=Masc|Number=Plur	9	amod	_	start_char=84|end_char=91
9	jezike	jezik	NOUN	Ncmpa	Case=Acc|Gender=Masc|Number=Plur	7	obj	_	start_char=92|end_char=98
10	,	,	PUNCT	Z	_	14	punct	_	start_char=98|end_char=99
11	hkrati	hkrati	ADV	Rgp	Degree=Pos	14	advmod	_	start_char=100|end_char=106
12	pa	pa	CCONJ	Cc	_	14	advmod	_	start_char=107|end_char=109
13	propedevtično	propedevtično	ADV	Rgp	Degree=Pos	14	advmod	_	start_char=110|end_char=123
14	obogati	obogati	VERB	Vmer3s	Aspect=Perf|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	parataxis	_	start_char=124|end_char=131
15	učno	učen	ADJ	Agpfsa	Case=Acc|Degree=Pos|Gender=Fem|Number=Sing	16	amod	_	start_char=132|end_char=136
16	sposobnost	sposobnost	NOUN	Ncfsa	Case=Acc|Gender=Fem|Number=Sing	14	obj	_	start_char=137|end_char=147
17	za	za	ADP	Sa	Case=Acc	19	case	_	start_char=148|end_char=150
18	vse	ves	DET	Pg-mpa	Case=Acc|Gender=Masc|Number=Plur|PronType=Tot	19	det	_	start_char=151|end_char=154
19	jezike	jezik	NOUN	Ncmpa	Case=Acc|Gender=Masc|Number=Plur	16	nmod	_	start_char=155|end_char=161
20	.	.	PUNCT	Z	_	7	punct	_	start_char=161|end_char=162

# text = Otrok naj bi se pred desetim letom starosti v šoli učil le maternega jezika (neformalno se nauči še pogovornega jezika doma, na ulici, v soseščini itd.) in umetnega jezika esperanta.
# sent_id = 2
1	Otrok	otrok	NOUN	Ncmsn	Case=Nom|Gender=Masc|Number=Sing	11	nsubj	_	start_char=163|end_char=168
2	naj	naj	PART	Q	_	11	advmod	_	start_char=169|end_char=172
3	bi	biti	AUX	Va-c	Mood=Cnd|VerbForm=Fin	11	aux	_	start_char=173|end_char=175
4	se	se	PRON	Px------y	PronType=Prs|Reflex=Yes|Variant=Short	11	expl	_	start_char=176|end_char=178
5	pred	pred	ADP	Si	Case=Ins	7	case	_	start_char=179|end_char=183
6	desetim	deseti	ADJ	Mlonsi	Case=Ins|Gender=Neut|NumType=Ord|Number=Sing	7	amod	_	start_char=184|end_char=191
7	letom	leto	NOUN	Ncnsi	Case=Ins|Gender=Neut|Number=Sing	11	obl	_	start_char=192|end_char=197
8	starosti	starost	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	7	nmod	_	start_char=198|end_char=206
9	v	v	ADP	Sl	Case=Loc	10	case	_	start_char=207|end_char=208
10	šoli	šola	NOUN	Ncfsl	Case=Loc|Gender=Fem|Number=Sing	11	obl	_	start_char=209|end_char=213
11	učil	učiti	VERB	Vmpp-sm	Aspect=Imp|Gender=Masc|Number=Sing|VerbForm=Part	0	root	_	start_char=214|end_char=218
12	le	le	PART	Q	_	11	advmod	_	start_char=219|end_char=221
13	maternega	materen	ADJ	Agpmsg	Case=Gen|Degree=Pos|Gender=Masc|Number=Sing	14	amod	_	start_char=222|end_char=231
14	jezika	jezik	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	11	obj	_	start_char=232|end_char=238
15	(	(	PUNCT	Z	_	18	punct	_	start_char=239|end_char=240
16	neformalno	neformalno	ADV	Rgp	Degree=Pos	18	advmod	_	start_char=240|end_char=250
17	se	se	PRON	Px------y	PronType=Prs|Reflex=Yes|Variant=Short	18	expl	_	start_char=251|end_char=253
18	nauči	naučiti	VERB	Vmer3s	Aspect=Perf|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	11	parataxis	_	start_char=254|end_char=259
19	še	še	PART	Q	_	18	advmod	_	start_char=260|end_char=262
20	pogovornega	pogovoren	ADJ	Agpmsg	Case=Gen|Degree=Pos|Gender=Masc|Number=Sing	21	amod	_	start_char=263|end_char=274
21	jezika	jezik	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	18	obj	_	start_char=275|end_char=281
22	doma	dom	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	21	nmod	_	start_char=282|end_char=286
23	,	,	PUNCT	Z	_	25	punct	_	start_char=286|end_char=287
24	na	na	ADP	Sl	Case=Loc	25	case	_	start_char=288|end_char=290
25	ulici	ulica	NOUN	Ncfsl	Case=Loc|Gender=Fem|Number=Sing	21	appos	_	start_char=291|end_char=296
26	,	,	PUNCT	Z	_	28	punct	_	start_char=296|end_char=297
27	v	v	ADP	Sl	Case=Loc	28	case	_	start_char=298|end_char=299
28	soseščini	soseščina	NOUN	Ncfsl	Case=Loc|Gender=Fem|Number=Sing	25	conj	_	start_char=300|end_char=309
29	itd.	itd.	X	Y	Abbr=Yes	28	nmod	_	start_char=310|end_char=314
30	)	)	PUNCT	Z	_	28	punct	_	start_char=314|end_char=315
31	in	in	CCONJ	Cc	_	33	cc	_	start_char=316|end_char=318
32	umetnega	umeten	ADJ	Agpmsg	Case=Gen|Degree=Pos|Gender=Masc|Number=Sing	33	amod	_	start_char=319|end_char=327
33	jezika	jezik	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	21	conj	_	start_char=328|end_char=334
34	esperanta	esperant	NOUN	Ncmsg	Case=Gen|Gender=Masc|Number=Sing	33	nmod	_	start_char=335|end_char=344
35	.	.	PUNCT	Z	_	11	punct	_	start_char=344|end_char=345

# text = Kasneje se lahko uči še kak tuj jezik (za poklic, za sporazumevanje z manjšino ali sosednjo etnijo itd.).
# sent_id = 3
1	Kasneje	kasno	ADV	Rgc	Degree=Cmp	4	advmod	_	start_char=346|end_char=353
2	se	se	PRON	Px------y	PronType=Prs|Reflex=Yes|Variant=Short	4	expl	_	start_char=354|end_char=356
3	lahko	lahko	ADV	Rgp	Degree=Pos	4	advmod	_	start_char=357|end_char=362
4	uči	učiti	VERB	Vmpr3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	start_char=363|end_char=366
5	še	še	PART	Q	_	4	advmod	_	start_char=367|end_char=369
6	kak	kak	DET	Pq-msn	Case=Nom|Gender=Masc|Number=Sing|PronType=Int	8	det	_	start_char=370|end_char=373
7	tuj	tuj	ADJ	Agpmsnn	Case=Nom|Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	8	amod	_	start_char=374|end_char=377
8	jezik	jezik	NOUN	Ncmsn	Case=Nom|Gender=Masc|Number=Sing	4	nsubj	_	start_char=378|end_char=383
9	(	(	PUNCT	Z	_	11	punct	_	start_char=384|end_char=385
10	za	za	ADP	Sa	Case=Acc	11	case	_	start_char=385|end_char=387
11	poklic	poklic	NOUN	Ncmsan	Animacy=Inan|Case=Acc|Gender=Masc|Number=Sing	8	appos	_	start_char=388|end_char=394
12	,	,	PUNCT	Z	_	14	punct	_	start_char=394|end_char=395
13	za	za	ADP	Sa	Case=Acc	14	case	_	start_char=396|end_char=398
14	sporazumevanje	sporazumevanje	NOUN	Ncnsa	Case=Acc|Gender=Neut|Number=Sing	11	conj	_	start_char=399|end_char=413
15	z	z	ADP	Si	Case=Ins	16	case	_	start_char=414|end_char=415
16	manjšino	manjšina	NOUN	Ncfsi	Case=Ins|Gender=Fem|Number=Sing	14	nmod	_	start_char=416|end_char=424
17	ali	ali	CCONJ	Cc	_	19	cc	_	start_char=425|end_char=428
18	sosednjo	sosednji	ADJ	Agpfsi	Case=Ins|Degree=Pos|Gender=Fem|Number=Sing	19	amod	_	start_char=429|end_char=437
19	etnijo	etnija	NOUN	Ncfsi	Case=Ins|Gender=Fem|Number=Sing	16	conj	_	start_char=438|end_char=444
20	itd	itd	PUNCT	Z	_	11	punct	_	start_char=445|end_char=448
21	.	.	PUNCT	Z	_	4	punct	_	start_char=448|end_char=449
22	)	)	PUNCT	Z	_	11	punct	_	start_char=449|end_char=450
23	.	.	PUNCT	Z	_	4	punct	_	start_char=450|end_char=451

# text = Dodatni tuji jeziki so stvar izbire in potrebe v konkretni situaciji, ne pa vsiljen balast.
# sent_id = 4
1	Dodatni	dodaten	ADJ	Agpmpn	Case=Nom|Degree=Pos|Gender=Masc|Number=Plur	3	amod	_	start_char=452|end_char=459
2	tuji	tuj	ADJ	Agpmpn	Case=Nom|Degree=Pos|Gender=Masc|Number=Plur	3	amod	_	start_char=460|end_char=464
3	jeziki	jezik	NOUN	Ncmpn	Case=Nom|Gender=Masc|Number=Plur	5	nsubj	_	start_char=465|end_char=471
4	so	biti	AUX	Va-r3p-n	Mood=Ind|Number=Plur|Person=3|Polarity=Pos|Tense=Pres|VerbForm=Fin	5	cop	_	start_char=472|end_char=474
5	stvar	stvar	NOUN	Ncfsn	Case=Nom|Gender=Fem|Number=Sing	0	root	_	start_char=475|end_char=480
6	izbire	izbira	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	5	nmod	_	start_char=481|end_char=487
7	in	in	CCONJ	Cc	_	8	cc	_	start_char=488|end_char=490
8	potrebe	potreba	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	6	conj	_	start_char=491|end_char=498
9	v	v	ADP	Sl	Case=Loc	11	case	_	start_char=499|end_char=500
10	konkretni	konkreten	ADJ	Agpfsl	Case=Loc|Degree=Pos|Gender=Fem|Number=Sing	11	amod	_	start_char=501|end_char=510
11	situaciji	situacija	NOUN	Ncfsl	Case=Loc|Gender=Fem|Number=Sing	8	nmod	_	start_char=511|end_char=520
12	,	,	PUNCT	Z	_	16	punct	_	start_char=520|end_char=521
13	ne	ne	PART	Q	Polarity=Neg	16	cc	_	start_char=522|end_char=524
14	pa	pa	CCONJ	Cc	_	13	fixed	_	start_char=525|end_char=527
15	vsiljen	vsiljen	ADJ	Appmsnn	Case=Nom|Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing|VerbForm=Part	16	amod	_	start_char=528|end_char=535
16	balast	balast	NOUN	Ncmsn	Case=Nom|Gender=Masc|Number=Sing	11	appos	_	start_char=536|end_char=542
17	.	.	PUNCT	Z	_	5	punct	_	start_char=542|end_char=543


</comment_241363>
<comment_241429 alignment='In favor' langue='sl'>
# text = Esperanto je praktičen in pravičen.
# sent_id = 0
1	Esperanto	Esperanta	PROPN	Npmsn	Case=Nom|Gender=Masc|Number=Sing	3	nsubj	_	start_char=0|end_char=9
2	je	biti	AUX	Va-r3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Pres|VerbForm=Fin	3	cop	_	start_char=10|end_char=12
3	praktičen	praktičen	ADJ	Agpmsnn	Case=Nom|Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	0	root	_	start_char=13|end_char=22
4	in	in	CCONJ	Cc	_	5	cc	_	start_char=23|end_char=25
5	pravičen	pravičen	ADJ	Agpmsnn	Case=Nom|Definite=Ind|Degree=Pos|Gender=Masc|Number=Sing	3	conj	_	start_char=26|end_char=34
6	.	.	PUNCT	Z	_	3	punct	_	start_char=34|end_char=35

# text = EU rabi svoj skupni jezik, da bo komuniciranje lažje in enakopravno.
# sent_id = 1
1	EU	EU	PROPN	Npfsn	Case=Nom|Gender=Fem|Number=Sing	2	nsubj	_	start_char=36|end_char=38
2	rabi	rabiti	VERB	Vmpr3s	Aspect=Imp|Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	start_char=39|end_char=43
3	svoj	svoj	DET	Px-msa	Case=Acc|Gender=Masc|Number=Sing|Poss=Yes|PronType=Prs|Reflex=Yes	5	det	_	start_char=44|end_char=48
4	skupni	skupen	ADJ	Agpmsay	Case=Acc|Definite=Def|Degree=Pos|Gender=Masc|Number=Sing	5	amod	_	start_char=49|end_char=55
5	jezik	jezik	NOUN	Ncmsan	Animacy=Inan|Case=Acc|Gender=Masc|Number=Sing	2	obj	_	start_char=56|end_char=61
6	,	,	PUNCT	Z	_	10	punct	_	start_char=61|end_char=62
7	da	da	SCONJ	Cs	_	10	mark	_	start_char=63|end_char=65
8	bo	biti	AUX	Va-f3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Fut|VerbForm=Fin	10	cop	_	start_char=66|end_char=68
9	komuniciranje	komuniciranje	NOUN	Ncnsn	Case=Nom|Gender=Neut|Number=Sing	10	nsubj	_	start_char=69|end_char=82
10	lažje	lahek	ADJ	Agcnsn	Case=Nom|Degree=Cmp|Gender=Neut|Number=Sing	5	acl	_	start_char=83|end_char=88
11	in	in	CCONJ	Cc	_	12	cc	_	start_char=89|end_char=91
12	enakopravno	enakopraven	ADJ	Agpnsn	Case=Nom|Degree=Pos|Gender=Neut|Number=Sing	10	conj	_	start_char=92|end_char=103
13	.	.	PUNCT	Z	_	2	punct	_	start_char=103|end_char=104

# text = Prihod novih držav in etnij v EU ne bo povzročal jezikovnih problemov.
# sent_id = 2
1	Prihod	prihod	NOUN	Ncmsn	Case=Nom|Gender=Masc|Number=Sing	10	nsubj	_	start_char=105|end_char=111
2	novih	nov	ADJ	Agpfpg	Case=Gen|Degree=Pos|Gender=Fem|Number=Plur	3	amod	_	start_char=112|end_char=117
3	držav	država	NOUN	Ncfpg	Case=Gen|Gender=Fem|Number=Plur	1	nmod	_	start_char=118|end_char=123
4	in	in	CCONJ	Cc	_	5	cc	_	start_char=124|end_char=126
5	etnij	etnija	NOUN	Ncfpg	Case=Gen|Gender=Fem|Number=Plur	3	conj	_	start_char=127|end_char=132
6	v	v	ADP	Sa	Case=Acc	7	case	_	start_char=133|end_char=134
7	EU	EU	PROPN	Npfsa	Case=Loc|Gender=Fem|Number=Sing	5	nmod	_	start_char=135|end_char=137
8	ne	ne	PART	Q	Polarity=Neg	10	advmod	_	start_char=138|end_char=140
9	bo	biti	AUX	Va-f3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Fut|VerbForm=Fin	10	aux	_	start_char=141|end_char=143
10	povzročal	povzročati	VERB	Vmpp-sm	Aspect=Imp|Gender=Masc|Number=Sing|VerbForm=Part	0	root	_	start_char=144|end_char=153
11	jezikovnih	jezikoven	ADJ	Agpmpg	Case=Gen|Degree=Pos|Gender=Masc|Number=Plur	12	amod	_	start_char=154|end_char=164
12	problemov	problem	NOUN	Ncmpg	Case=Gen|Gender=Masc|Number=Plur	10	obj	_	start_char=165|end_char=174
13	.	.	PUNCT	Z	_	10	punct	_	start_char=174|end_char=175

# text = Esperanto bo zaradi svojih lastnosti uspešno zastopal EU po vsem svetu in bo branil "majhne" jezike pred terorjem angleščine, ruščine, kitajščine, arabščine itd.
# sent_id = 3
1	Esperanto	esperanta	NOUN	Ncmsn	Case=Nom|Gender=Masc|Number=Sing	7	nsubj	_	start_char=176|end_char=185
2	bo	biti	AUX	Va-f3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Fut|VerbForm=Fin	7	aux	_	start_char=186|end_char=188
3	zaradi	zaradi	ADP	Sg	Case=Gen	5	case	_	start_char=189|end_char=195
4	svojih	svoj	DET	Px-fpg	Case=Gen|Gender=Fem|Number=Plur|Poss=Yes|PronType=Prs|Reflex=Yes	5	det	_	start_char=196|end_char=202
5	lastnosti	lastnost	NOUN	Ncfpg	Case=Gen|Gender=Fem|Number=Plur	7	obl	_	start_char=203|end_char=212
6	uspešno	uspešno	ADV	Rgp	Degree=Pos	7	advmod	_	start_char=213|end_char=220
7	zastopal	zastopati	VERB	Vmpp-sm	Aspect=Imp|Gender=Masc|Number=Sing|VerbForm=Part	0	root	_	start_char=221|end_char=229
8	EU	EU	PROPN	Npfsa	Case=Acc|Gender=Fem|Number=Sing	7	obj	_	start_char=230|end_char=232
9	po	po	ADP	Sl	Case=Loc	11	case	_	start_char=233|end_char=235
10	vsem	ves	DET	Pg-msl	Case=Loc|Gender=Masc|Number=Sing|PronType=Tot	11	det	_	start_char=236|end_char=240
11	svetu	svet	NOUN	Ncmsl	Case=Loc|Gender=Masc|Number=Sing	7	obl	_	start_char=241|end_char=246
12	in	in	CCONJ	Cc	_	14	cc	_	start_char=247|end_char=249
13	bo	biti	AUX	Va-f3s-n	Mood=Ind|Number=Sing|Person=3|Polarity=Pos|Tense=Fut|VerbForm=Fin	14	aux	_	start_char=250|end_char=252
14	branil	braniti	VERB	Vmpp-sm	Aspect=Imp|Gender=Masc|Number=Sing|VerbForm=Part	7	conj	_	start_char=253|end_char=259
15	"	"	PUNCT	Z	_	16	punct	_	start_char=260|end_char=261
16	majhne	majhen	ADJ	Agpmpa	Case=Acc|Degree=Pos|Gender=Masc|Number=Plur	18	amod	_	start_char=261|end_char=267
17	"	"	PUNCT	Z	_	16	punct	_	start_char=267|end_char=268
18	jezike	jezik	NOUN	Ncmpa	Case=Acc|Gender=Masc|Number=Plur	14	obj	_	start_char=269|end_char=275
19	pred	pred	ADP	Si	Case=Ins	20	case	_	start_char=276|end_char=280
20	terorjem	teror	NOUN	Ncmsi	Case=Ins|Gender=Masc|Number=Sing	18	nmod	_	start_char=281|end_char=289
21	angleščine	angleščina	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	20	nmod	_	start_char=290|end_char=300
22	,	,	PUNCT	Z	_	23	punct	_	start_char=300|end_char=301
23	ruščine	ruščina	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	21	conj	_	start_char=302|end_char=309
24	,	,	PUNCT	Z	_	25	punct	_	start_char=309|end_char=310
25	kitajščine	kitajščina	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	20	conj	_	start_char=311|end_char=321
26	,	,	PUNCT	Z	_	27	punct	_	start_char=321|end_char=322
27	arabščine	arabščina	NOUN	Ncfsg	Case=Gen|Gender=Fem|Number=Sing	20	conj	_	start_char=323|end_char=332
28	itd	itd	PROPN	Npfsg	Case=Gen|Gender=Fem|Number=Sing	27	nmod	_	start_char=333|end_char=336
29	.	.	PUNCT	Z	_	7	punct	_	start_char=336|end_char=337


</comment_241429>
