import sys

vp = 0
fp = 0
vn = 0
fn = 0

for line in sys.stdin:
    line = line.strip().split("\t")
    if line[0] == "comment_id":
        continue

    alignment = line[6]
    prediction = line[7]

    if alignment == prediction:
        if alignment == "In favor":
            vp += 1
        else:
            vn += 1
    else:
        if prediction == "In favor":
            fp += 1
        else:
            fn += 1

p_i = (vp)/(vp+fp)
p_a = (vn)/(vn+fn)
r_i = (vp)/(vp+fn)
r_a = (vn)/(vn+fp)
print(f"Précision In favor = {p_i}")
print(f"Précision Against = {p_a}")
print(f"Rappel In favor = {r_i}")
print(f"Rappel Against = {r_a}")
print(f"F-mesure In favor = {2*((p_i*r_i)/(p_i+r_i))}")
print(f"F-mesure Against = {2*((p_a*r_a)/(p_a+r_a))}")